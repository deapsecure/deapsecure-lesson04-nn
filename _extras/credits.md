---
title: "Credits for materials used"
---

## Image credits

* Image of network of neurons, posted at Max Pixel

  https://www.maxpixel.net/Neurons-Brain-Structure-Network-Brain-Cells-Brain-440660
  This image is in public domain. license of Creative Commons Zero CC0. Use this image
  with referral.

* Image of a neuron, by Wikipedia user `BruceBlaus`

  https://en.wikipedia.org/wiki/Neuron#/media/File:Blausen_0657_MultipolarNeuron.png
  Author is BruceBlaus. This work is free and may use by anyone.
  CC BY 3.0 <https://creativecommons.org/licenses/by/3.0/>

* Color image of a fully-connected neural network with one hidden layer,
  by Wikimedia user `Glosser.ca`

  <https://en.wikipedia.org/wiki/Artificial_neural_network#/media/File:Colored_neural_network.svg>
  <https://commons.wikimedia.org/wiki/File:Colored_neural_network.svg>
  CC BY-SA 3.0

  Modified by WP to match the fill color to conventions used by Asimov Institute's
  [neural network zoo](http://www.asimovinstitute.org/neural-network-zoo/).

* Overfitting illustration,
  by Wikimedia user `Chabacano`

  <https://en.wikipedia.org/wiki/Overfitting#/media/File:Overfitting.svg>
  CC BY-SA 4.0
  This image is free to share and adapt.

* Dropout illustration

  Srivastava, N., Hinton, G., Krizhevsky, A., Sutskever, I., & Salakhutdinov, R. (2014). Dropout: a simple way to prevent neural networks from overfitting. The journal of machine learning research, 15(1), 1929-1958.

* Resnet introduction

  He, K., Zhang, X., Ren, S., & Sun, J. (2016). Deep residual learning for image recognition. In Proceedings of the IEEE conference on computer vision and pattern recognition (pp. 770-778).
  
{% include links.md %}
