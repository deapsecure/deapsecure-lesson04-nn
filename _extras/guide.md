---
title: "Instructor Notes"
---

## Lessons in Neural Networks & HPC

This lesson module serves several important purposes:

  1. Introduce learners to the basic concepts of neural networks
     for classification tasks,
     and the basic procedure to construct, train, evaluate, and improve
     the neural network models.

  2. Reinforce the initial data exploration, cleaning, and preprocessing
     with a new dataset, `sherlock_18apps`
     (which nevertheless bears a lot of similarities to the "2-apps" dataset).

  3. Learn the process of converting an interactive Jupyter analysis
     to a noninteractive script.

This lesson would truly bridge people from the "interactive computing
mode" on Jupyter platform to the "batch noninteractive computing" on
HPC with job scheduler.



{% include links.md %}
