DeapSECURE Extra Lesson Snippets - Neural Networks module
=========================================================

This directory contains extra snippets of lesson texts
that are reserved for future uses,
or were removed from the current version of the lesson because they are tangential
or too confusing / complicated for new learners.
They were saved here in order of keep these snippets in mind.

