---
title: "Task: Recognition of Malicious URLs"
teaching: 0
exercises: 0
questions:
- "How do we train a neural network which can detect whether a URL is malicious?"
objectives:
- "First learning objective. (FIXME)"
keypoints:
- "First key point. Brief Answer to questions. (FIXME)"
---

Our task in this workshop is to train a neural network to detect malicious
URLs.
We will follow
[a worked example presented by Github user `incertum`](
    https://github.com/incertum/cyber-matrix-ai/blob/master/Malicious-URL-Detection-Deep-Learning/FeaturelessDeepLearningMaliciousURLClassification.ipynb
)
(Mellissa C. Kilby,
[see her website](http://www.melissackilby.com/)).
She also has [an article](
    https://github.com/incertum/cyber-matrix-ai/tree/master/Malicious-URL-Detection-Deep-Learning
) which explains in more detail her work.

> ## Open the websites
>
> Please open these two websites in separate tabs; we will refer to them
> from time to time.
>
> * [Jupyter notebook, `FeaturelessDeepLearningMaliciousURLClassification.ipynb`](
>       https://github.com/incertum/cyber-matrix-ai/blob/master/Malicious-URL-Detection-Deep-Learning/FeaturelessDeepLearningMaliciousURLClassification.ipynb
>   )
>
> * [Article describing the approach](
>       https://github.com/incertum/cyber-matrix-ai/tree/master/Malicious-URL-Detection-Deep-Learning
>   )
{:.challenge}

## Structure of the Program

In this episode, we will simply give a quick tour of the Python neural network program.
As can be seen in the Jupyter notebook above, the program is somewhat lengthy.
We have created a Python file named `url_analyzer.py` containing most
of the Python codes shown in the Jupyter notebook.
We will load to our IPython environment so that we can
go into the training phase as soon as possible.

The `url_analyzer.py` file contains the following pieces:

* An initialization part: loading all the necessary Python libraries and functions:

```python
# Load Libraries - Make sure to run this cell!
import pandas as pd
import numpy as np
import re, os
from string import printable
from sklearn import model_selection
...
```

* It also contains an important section to set the number of cores:

```python
tf_sess_conf = K.tf.ConfigProto(intra_op_parallelism_threads=1,
                                inter_op_parallelism_threads=1)
print(tf_sess_conf)
tf_sess = K.set_session(K.tf.Session(config=tf_sess_conf))
K.set_session(tf_sess)
```

The two numbers `1` refer to the number of CPU cores we will be using.
This will need to be set to the correct number of cores *before*
we load this file into Python.

* A section which loads the CSV data into memory and prepares them for
machine learning.

* Several helpful functions:

  * `print_layers_dims`: Prints the layers of the network and their dimensions.

  * `save_model`: A function to save the trained model to disk for reuse later.

  * `load_model`: A function to load a previously trained model (for further
    training or for inference).

  * `simple_lstm`: Contains the definition of the "LSTM" neural network (model #1).

  In addition, the Jupyter notebook contains two more models which you can try:

  *  `lstm_conv`: Contains the definition of the "LSTM + 1D convolution"
     neural network (model #2).

  * `conv_fully`: Contains the definition of the "1D convolution + fully connected"
     neural network (model #3).


## Loading the Python File

Please invoke the following command to load up these steps and functions now, on the
IPython session:

    >>> exec(open("url_analyzer.py").read())

It reads the contents of `url_analyzer.py` and executes it in the current Python
session.
It will print some samples of the URLs as shown below.


## Peeking Into the Dataset

The dataset used to train the network has almost 200k entries, which are labeled
as benign (value zero) or malicious (value 1). Here are 25 entries chosen randomly:

    Sample data:
                                                          url  isMalicious
    164928             paletteswapninja.com/~playre5/0mxupm8q            1
    144540                                   chromeupdate.top            1
    183300                                          rrsee.com            1
    15126   netdoctor.co.uk/plus.google.com/share?url=http...            0
    97885                      pik2sedayu.com/casts/index.php            1
    6       datacenterdynamics.com/awards/latin-america-aw...            0
    7775              nethtml/2017-03/15/content_40957310.htm            0
    87768   ly.com/www.ly.com/youlun/tours-142418.html#Res...            0
    165586                     uorenbuffets.com/9828/s4d5.php            1
    159900                              control3.com.br/vnw51            1
    41801           elpais.com/elpais.com/tag/roland_garros/a            0
    55278                 seattletimes.com/author/jerry-large            0
    45112    exam8.com/file/gongzuo/geren/201703/3979488.html            0
    70933                          metacpan.org/module/Twiggy            0
    69557                                  alltop.com/alpha/u            0
    91959                           decorpad.com/paint/greens            0
    138037                            muongcaupo.net/khdym6am            1
    8834    strandbooks.com/lower%2Dpriced%2Dthan%2Debooks...            0
    170133                      hilanguage.com.tw/m/index.htm            1
    46921   futureoflife.org/2015/10/12/elon-musk-donates-...            0
    71020   parksmalayer.ir/index.php/2014-06-01-09-26-58/...            0
    137046                            information00paypal.com            1
    107142  deli-info.pl/mod/wells/cmd-login=2309492d735f9...            1
    127364  boostyourparty.com/plugins/jfbcprofiles/login....            1
    193967                                    1drv.ms/1Uq7OpT            1
    Matrix dimensions of X:  (194798, 75) Vector dimension of target:  (194798,)

Can you recognize which sites are malicious and which are not, just by looking at the link?

For the machine learning, these URLs are encoded and truncated to a maximum of 75 characters,
then transformed into vectors (using a technique called `word2vec`) for efficient processing.


## Splitting Training Data

In the data preparation stage, there is a very important step which is splitting the
training data into at least two partitions:

1. **Training** data

2. **Cross-validation** data

3. (Recommended) **Testing** data

Here's the Python code that accomplishes that:

```python
# Simple Cross-Validation: Split the data set into training and test data
X_train, X_test, target_train, target_test = \
    model_selection.train_test_split(X, target, test_size=0.25, random_state=33)
```

25% of the data is used for test/validation data, and the remaining 75% for training.
The composition of training : cross-validation : testing data varies depending on area of
practice.
One typical example would be 80% : 10% : 10%.

Data in these partitions must not be mixed.
The network is trained using the **training** data to obtain the optimal network parameters.
Then the network is cross-validated using the second set of data to ensure that the network
generalizes well (i.e. bias-variance balance).
These training and cross-validation steps are often repeated until the best network is obtained.
Then it is finally validated ("tested") using the **testing** data.
In our program, we only do one training step and one cross-validation step.
You are welcome to explore the testing step by further splitting the data.



## Defining Neural Network in KERAS

Here is model #1 defined in KERAS:

```python
def simple_lstm(max_len=75, emb_dim=32, max_vocab_len=100, lstm_output_size=32, W_reg=regularizers.l2(1e-4)):
    # Input
    main_input = Input(shape=(max_len,), dtype='int32', name='main_input')
    # Embedding layer
    emb = Embedding(input_dim=max_vocab_len, output_dim=emb_dim, input_length=max_len,
                dropout=0.2, W_regularizer=W_reg)(main_input)

    # LSTM layer
    lstm = LSTM(lstm_output_size)(emb)
    lstm = Dropout(0.5)(lstm)

    # Output layer (last fully connected layer)
    output = Dense(1, activation='sigmoid', name='output')(lstm)

    # Compile model and define optimizer
    model = Model(input=[main_input], output=[output])
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model
```

KERAS network definition is quite intuitive: Starting from the input layer (`main_input`),
it passes through an embedding layer (`emb`).
Following it, there is LSTM and dropout (`lstm`).
The final output layer is one fully connected layer with just one output value
(thus, `Dense(1, ...`).
The output value is supposed to be close to 1 for a malicious URL
and close to 0 for a benign URL.
We can see that the Sigmoid activation function is used.
Finally, the network will be optimized using "Adam optimizer" during the training phase.

Here is a schematic diagram of how the network looks like:

![Diagram for network 1 (simple_lstm)](../fig/Net-diagram-model01.png)

KERAS' model is actually an expression tree.
It is not evaluated immediately; for example, the `Input` layer has only the placeholder
for what will be the input values.
Behind the scenes, KERAS and TensorFlow transform this network expression to efficient
computation tasks in the training or inference phases.



{% include links.md %}

